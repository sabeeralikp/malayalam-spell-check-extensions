# Malayalam Spell Check Extensions

Malayalam Spell Check is an open-source browser extension that checks and
rewrites Malayalam text. It works with mozilla and chromium based web
browsers.<br>
<br>
<b>Note:</b> This is product is in a pre-alpha stage

## Installation

### Download the extension package for your browser.<br>
   <br>
   <b>Chromium Users (Chrome, Edge, Brave, Opera): </b><br>[Download Chromium Malayalam Spell Check Extension (Click Here)](https://gitlab.com/sabeeralikp/malayalam-spell-check-extensions/-/blob/main/Malayalam_Spell_Checker_Extension.zip) and extract it.<br><br>
   <b>Firefox Users(Mozilla Firefox, Waterfox, Firefox Focus, ...): </b><br>
   [Download Firefox Malayalam Spell Check Extension (Click Here)](https://gitlab.com/sabeeralikp/malayalam-spell-check-extensions/-/blob/main/Firefox_Malayalam_Spell_Checker.xpi)
<br><br>
Follow the following steps to install it into your browser

### 1. Enable Developer Options [Only for Chromium Users (Chrome, Edge, Brave, Opera)]


Enable Developer options on your browser<br><br>
<b>Chromium Users(Chrome, Edge, Brave, Opera): </b><br>

1. [Go to Manage Extension Window (Click Here)](chrome://extensions/)<br><br>
2. Turn on the developer mode option <br>
   <br>
   (Chrome)<br>
   <img src="images/chrome_dev.jpg">
   (Edge)
   <img src="images/edge_dev.jpg">

### 2. Drag and Drop Your Desired Extension File

Download the extension package for your browser.<br>
<br>
<b>Chromium Users (Chrome, Edge, Brave, Opera): </b><br>

1. Drag and drop [Chromium Extension File](https://gitlab.com/sabeeralikp/malayalam-spell-check-extensions/-/blob/main/Malayalam_Spell_Checker_Extension.crx) to the [Extension Window (Click Here)](chrome://extensions/)<br>
   (Chrome)
   <img src="images/chrome_drag_and_drop.jpg"><br>
   (Edge)
   <img src="images/edge_drag_and_drop.jpg"><br>
   OR<br>
   Drag and drop [Firefox Malayalam Spell Check Extension](https://gitlab.com/sabeeralikp/malayalam-spell-check-extensions/-/blob/main/Firefox_Malayalam_Spell_Checker.xpi) in the firefox [addons window (Click here)](about:addons)<br>
   (Firefox)
   <img src="images/firefox_drag_and_drop.jpg">
2. Click <b>"Add Extension"</b> OR <b>"Add"</b> <br> <br>


## Usage

### 1. Go to any website and highlight any malayalam text

<img src="images/step_1.jpg">
<br>

### 2. Click the incorrect word highlighted in red

<img src="images/step_2.jpg">

### 3. Select correct word from suggestions given

<img src="images/step_2_2.jpg">

### 4. Copy the corrected text by clicking the "Copy Button"

<img src="images/step_4.jpg"><br><br>

### Note: if there is any issue with installation or usage of extension raise it in [issues](https://gitlab.com/sabeeralikp/malayalam-spell-check-extensions/-/issues)<br>

Thanks 😊
   
